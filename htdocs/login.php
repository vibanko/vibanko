<?php
require_once("common.php");

@session_start();

if(isset($_POST['handle'],$_POST['password'],$_POST['token']))
{
    if($_POST['token'] === $_SESSION['csrf_token'])
    {
        if(preg_match('/[A-Za-z0-9]+/',$_POST['handle']))
        {
            $stmt = $db->prepare("SELECT
                                      COUNT(attempt_ip) AS count
                                  FROM login_attempts
                                  WHERE attempt_ip=:ip
                                  AND attempt_timestamp > CURRENT_TIMESTAMP - interval '10 minutes'");
            
            $stmt->bindValue('ip',$_SERVER['REMOTE_ADDR'],PDO::PARAM_STR);
            
            $stmt->execute();
            
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            
            if($result === False)
            {
                throw new Exception("SQL fail");
            }
            
            if($result['count'] < 3)
            {
                $stmt = $db->prepare("INSERT INTO login_attempts
                                      (
                                          attempt_ip,
                                          attempt_username
                                      ) VALUES (:ip,:handle)
                                      ");
                $stmt->bindValue('ip',$_SERVER['REMOTE_ADDR'],PDO::PARAM_STR);
                $stmt->bindValue('handle',$_POST['handle'],PDO::PARAM_STR);
                
                $stmt->execute();
                
                $stmt = $db->prepare('SELECT
                                          user_id,
                                          password_hash,
                                          password_salt,
                                          password_iterations,
                                          balance
                                      FROM users
                                      WHERE user_handle=:handle');

                $stmt->bindValue('handle',strtolower(trim($_POST['handle'])),PDO::PARAM_STR);
                
                $stmt->execute();
                
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                
                if($result === False)
                {
                    redirect("failed to login.");
                }
                else
                {
                    $salt = stream_get_contents($result['password_salt']);
                    $iterations = $result['password_iterations'];
                    $hash = calculate_hash($_POST['password'],$salt,$iterations);
                    
                    if($hash === stream_get_contents($result['password_hash']))
                    {
                        $_SESSION['user_id'] = $result['user_id'];
                        redirect('Successfully logged in');
                    }
                    else
                    {
                        redirect("Failed to login.");
                    }
                }
            }
            else
            {
                redirect("Brute force detected, please wait 10 minutes to try again.");
            }
        }
        else
        {
            redirect("Handle must be alpha numeric.");
        }
    }
    else
    {
        redirect("Token mismatch");
    }
}
else
{
    redirect("Missing parameters");
}

