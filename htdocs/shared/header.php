<?php @session_start(); ?>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="style.css"> 
    <title>Vibanko</title>
</head>
<body>
<div id='container'>
<a href='/'><div id='header'></div></a>
<div id='sidebar'>
<div id='login' class="box">
    <?php
    require_once("common.php");
    if (isset($_SESSION['user_id'])) { ?>
        <h1>Welcome</h1>
        <h3><a href="/">Home</a></h3>
        <p>Signed in as <?php echo get_handle();?>. <a href='logout.php'>Logout</a>
    <?php } else { ?>
        <h1>Login</h1>
        <form action="login.php" method="post">
            <label>Login:</label>
            <input type='text' name="handle"/>
            <label>Password:</label>
            <input type='password' name="password"/>
            <input type='submit' value='Login' />
            <input type="hidden" name="token" value="<?php echo $_SESSION['csrf_token'];?>" />
        </form>
    <?php } ?>
</div>
<div id='links' class="box">
    <h1>Additional links</h1>
    <ul>
        <li><a href='http://www.bitcoinconsultancy.com'>Bitcoin Consultancy</a> <br/>Software development</li>
        <li><a href='https://www.britcoin.co.uk'>Buy bitcoins with GBP</a></li>
        <li><a href='https://intersango.com'>Buy bitcoins with EUR</a> </li>
        <li><a href='https://intersango.us'>Buy bitcoins with USD</a> </li>
    </ul>
</div>
<img id='intersango_logo' src='images/intersango.png' />
</div>

