<?php
@session_start();
require_once("common.php");

if(!isset($_SESSION['user_id']))
{
    redirect('Please <a href="login.php">login</a>');
}

function validate_input()
{
    if(preg_match('/^(\d*\.?\d+|\d+\.?\d*)$/',trim($_POST['amount'])) !== 1 )
    {
        redirect("That doesn't look like a number");
    }
    else
    {
        if(!check_precision($_POST['amount'],8))
        {
            redirect("Quantity limited to eight decimal places");
        }
        
        if(bccomp($_POST['amount'],"0.01",8) < 0)
        {
            redirect("Quantity cannot be less than 0.01 BTC");
        }
    }
    if($_POST['token'] !== $_SESSION['csrf_token'])
    {
        redirect("Token mismatch");
    }
}

if(isset($_POST['address'],$_POST['amount'],$_POST['token']))
{
    validate_input();
    
    $stmt = $db->prepare('SELECT balance
                          FROM users
                          WHERE user_id=:user_id
                          FOR UPDATE');

    $stmt->bindValue('user_id',$_SESSION['user_id'],PDO::PARAM_INT);
    
    $stmt->execute();
    
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    
    if($result === False)
    {
        redirect("Failed to get account balance");
    }
    else
    {
        $balance = $result['balance'];
        if(bccomp($balance,$_POST['amount'],8) < 0)
        {
            redirect("You do not have enough funds for that.");
        }
        else
        {
            $stmt = $db->prepare('INSERT INTO withdrawal_requests
                                  (
                                      withdrawal_amount,
                                      bitcoin_address,
                                      user_id
                                  ) VALUES(:amount,:address,:id)');
            
            $stmt->bindValue('amount',$_POST['amount'],PDO::PARAM_STR);
            $stmt->bindValue('address',$_POST['address'],PDO::PARAM_STR);
            $stmt->bindValue('id',$_SESSION['user_id'],PDO::PARAM_STR);
            
            $stmt->execute();
            
            $stmt = $db->prepare('UPDATE users
                                  SET balance=balance-:amount
                                  WHERE user_id=:id');
            
            $stmt->bindValue('amount',$_POST['amount'],PDO::PARAM_STR);
            $stmt->bindValue('id',$_SESSION['user_id'],PDO::PARAM_INT);
            
            $stmt->execute();
            
            redirect("successfully submitted withdrawal request");
        }
    }
}
else
{
    redirect("Missing parameters");
}
?>
