<?php require 'shared/header.php'; ?>
<div id='main' class="box">
    <h1>Welcome to Vibanko</h1>
    <?php if (isset($_SESSION['flash']))
    {
        echo "<p class='error'>{$_SESSION['flash']}</p>";
        unset($_SESSION['flash']);
    }
    ?>
    <p>The events of Mybitcoin are not uncommon in the bitcoin world. Many merchant sites dealing with other user's bitcoins have been and continue to be compromised; some sites repeatedly. <br/><br/>In response to requests by the bitcoin community, we have decided to offer a reliable and trustworthy bitcoin wallet service.</p>
    <?php if (isset($_SESSION['user_id'])) { ?>
    <table>
        <tr>
            <th>Login</th>
            <th>Bitcoins</th>
            <th>Pending<br />withdraws</th>
           </tr>
        <tr>
            <td><?php echo get_handle();?></td>
            <td><?php echo get_balance();?></td>
            <td><?php echo pending_withdrawals();?></td>
           </tr>
    </table>
</div>
<div id="deposit">
    <h1>Deposit</h1>
    <p>To deposit simply send funds to the address below. This address is unique to your account and will allow us to automatically connect the payment with your account.</p>
    <table>
        <tr>
            <th>Unique deposit address</th>
           </tr>
        <tr>
            <td><?php echo deposit_address();?></td>
           </tr>
    </table>
</div>
<div class="box" id="withdrawal">
    <h1>Withdraw</h1>
    <p>To withdraw funds simply copy paste the address you wish to send to, enter the amount and click 'Send'. Your bitcoins will be sent automatically.</p>
    <form action="withdrawal.php" method="post">
        <label for='address'>Address:</label>
        <input id='address' name="address" type='text' /> <br />
        <label for='amount'>Amount:</label>
        <input id='amount' name="amount" type='text' /> <br />
        <input type='submit' value='Send' />
        <input type="hidden" name="token" value="<?php echo $_SESSION['csrf_token'];?>" />
    </form>
    <?php } else { ?>
    <br />
</div>
<div id="register" class="box">
    <h1>Register</h1>
    <?php if (isset($_SESSION['flash']))
    {
        echo "<p class='error'>{$_SESSION['flash']}</p>";
        unset($_SESSION['flash']);
    }
    ?>
    <form action="register.php" method="post">
        <label for='username_register'>Username:</label>
        <input id='username_register' type="text" name="handle" /><br />
        <label for='password_register'>Password:</label>
        <input id='password_register' type="password" name="password" /> <br />
        <label for='password_register'>Confirm:</label>
        <input type="password" name="confirm_password" /><br />
        <label for='email_register'>Email:</label>
        <input id='email_register' type="text" name="email" /><br />
        <input type='submit' value='Register' />
        <input type="hidden" name="token" value="<?php echo $_SESSION['csrf_token'];?>" />
    </form>
    <?php } ?>
</div>
<?php require 'shared/footer.php';

