<?php
if(!isset($_SESSION['csrf_token']))
{
    $token = "";
    for($i=0;$i<16;$i++)
    {
        $token .= chr(mt_rand(ord('A'),ord('Z')));
    }
    
    $_SESSION['csrf_token'] = $token;
}

require_once("../config.php");

function pending_withdrawals()
{
    global $db;
    $stmt = $db->prepare("SELECT 
                              SUM(withdrawal_amount) AS amount
                          FROM withdrawal_requests
                          WHERE status='PENDING'
                          AND user_id=:user_id");

    $stmt->bindValue('user_id',$_SESSION['user_id'],PDO::PARAM_INT);

    $stmt->execute();

    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    
    if($result)
    {
        return $result['amount']===null?0:$result['amount'];
    }
    else
    {
        throw new Exception('Failed to get pending');
    }
}

function deposit_address()
{
    global $db;
    $stmt = $db->prepare('SELECT bitcoin_address
                          FROM bitcoin_addresses
                          WHERE bitcoin_address NOT IN (SELECT bitcoin_address FROM deposits)
                          AND user_id=:user_id
                          LIMIT 1');

    $stmt->bindValue('user_id',$_SESSION['user_id'],PDO::PARAM_INT);

    $stmt->execute();

    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    if($result === False)
    {
        $stmt = $db->prepare('UPDATE bitcoin_addresses
                              SET user_id=:id
                              WHERE
                              bitcoin_address=(SELECT bitcoin_address FROM bitcoin_addresses WHERE user_id IS NULL LIMIT 1)
                              RETURNING bitcoin_address');
        
        $stmt->bindValue('id',$_SESSION['user_id']);
        
        $stmt->execute();
        
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    if($result)
    {
        return $result['bitcoin_address'];
    }
    else
    {
        throw new Exception('Failed to get address');
    }
}

function redirect($message)
{
    @session_start();
    $_SESSION['flash'] = $message;
    header('HTTP/1.1 303 See Other');
    header('Location: /');
    exit(0);
}

function get_balance()
{
    global $db;
    if(!isset($_SESSION['user_id']))
    {
        throw new Exception("not logged in");
    }
    
    $stmt = $db->prepare('SELECT balance FROM users WHERE user_id=:id');
    $stmt->bindValue('id',$_SESSION['user_id'],PDO::PARAM_INT);
    $stmt->execute();
    
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    
    if($result === False)
    {
        throw new Exception("Failed to find handle");
    }
    
    return $result['balance'];
}

function get_handle()
{
    global $db;
    if(!isset($_SESSION['user_id']))
    {
        throw new Exception("not logged in");
    }
    
    $stmt = $db->prepare('SELECT user_handle FROM users WHERE user_id=:id');
    $stmt->bindValue('id',$_SESSION['user_id'],PDO::PARAM_INT);
    $stmt->execute();
    
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    
    if($result === False)
    {
        throw new Exception("Failed to find handle");
    }
    
    return $result['user_handle'];
}

// this function was taken from intersango v2
function check_precision($value,$places=5)
{
    /* This checks to make sure the value has less places after the decimal
     * than the $places parameter.  Unfortunately I cant think of any sane way
     * to do this so instead it's a string manipulation function.
     * assumes input is well formed, \d+\.\d+
     */
     
/* passed unit tests
echo check_precision("123.4567",4) === True?'Pass':'Fail '.__LINE__;echo "\n";
echo check_precision("123.456",4) === True?'Pass':'Fail '.__LINE__;echo "\n";
echo check_precision("123.456000",4) === True?'Pass':'Fail '.__LINE__;echo "\n";
echo check_precision("123.4567005",4) === False?'Pass':'Fail '.__LINE__;echo "\n";
echo check_precision("123.45675",4) === False?'Pass':'Fail '.__LINE__;echo "\n";
*/
     
    $value = trim($value,"0");
    
    $i = strpos($value,'.');
    
    if($i === False)
    {
        return True;
    }
    else
    {
        return strlen($value)-$i-1 <= $places;
    }
}

function generate_salt()
{
    $f = fopen('/dev/urandom','r');
    $salt = fread($f,8);
    fclose($f);
    return $salt;
}

function calculate_hash($password,$salt,$iterations)
{
    $hash = $password;
    for($i=0;$i<$iterations;$i++)
    {
        $hash = mhash(MHASH_SHA512,$hash,$salt);
    }
    return $hash;
}

