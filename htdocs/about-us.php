<?php require 'shared/header.php'; ?>
<div id='main' class="box">
    <h1>Welcome to Vibanko</h1>
    <p>
        We are the <a href="http://bitcoinconsultancy.com/">Bitcoin Consultancy</a>.
        <h3>Services</h3>
        <ul class='our-stuff'>
            <li><a href="https://britcoin.co.uk/">Britcoin.co.uk</a></li>
            <li><a href="https://intersango.com/">Intersango.com</a></li>
            <li><a href="https://intersango.us/">Intersango.us</a></li>
        </ul>
        <h3>Products</h3>
        <ul class='our-stuff'>
            <li><a href="https://gitorious.org/intersango/intersango">Intersango</a></li>
            <li><a href="https://gitorious.org/vibanko/vibanko">Vibanko</a></li>
        </ul>
    </p>

    <p>We can be contacted at:</p>
    <p>
    support@britcoin.co.uk
    </p>

    <p>
    <b>Intersango Ltd</b><br />
    3rd Floor 14 Hanover Street<br />
    London<br />
    England<br />
    W1S 1YH<br /> 
    </p>
</div>
<?php require 'shared/footer.php'; ?>
