<?php require 'shared/header.php'; ?>
<div id='main' class="box">
    <h1>Welcome to Vibanko</h1>
    <h3>Q. Why should I trust you?</h3>
    <p>A. We currently operate 3 bitcoin exchanges: <a href="https://britcoin.co.uk/">Britcoin</a>, <a href="https://intersango.com/">Intersango (EUR)</a> and <a href="https://intersango.us/">Intersango (USD).</a></p>
    <p>The source code for Vibanko is available for review on <a href="http://gitorious.org/vibanko/vibanko/">gitorious</a>.</p>
    <h3>Q. Are the bitcoins on deposit backed up off-site? How often?</h3>
    <p>A. An encrypted copy of the live wallet is stored in multiple physical locations.  Both online storage and offline.
    Additionally we will only be keeping enough funds in the live wallet to handle predicted daily turn over.
</p>
<p>
These are the same steps we take with britcoin and the intersango sites. (note that we do not run the brazilian exchange using the intersango software).</p>
    <h3>Q. How many people have access to the wallet? How do you know you can trust them, or how are they audited?</h3>
    <p>A. Only the system administrator has immediate access, however two other members of the Bitcoin Consultancy have access to backups of the live wallet and storage wallets should something happen to him.</p>
    <h3>Q. What happens if you decide Vibanko is not a viable business and has to be shut down?</h3>
    <p>A. We do not see this as being a likely outcome as the continued operation of the site incures minimal expenses.  However should a situation arise in which we have to stop operating deposits would be disabled and ViBanko would continue to operate at our expense until all funds had been withdrawn.</p>
</div>
<?php require 'shared/footer.php'; ?>
