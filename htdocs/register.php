<?php
require_once("common.php");

@session_start();

// input validation redirects on failure
if (!isset($_POST['handle'], $_POST['password'], $_POST['confirm_password'], $_POST['email'], $_POST['token']))
{
    redirect("Missing parameters");
}

if ($_POST['token'] !== $_SESSION['csrf_token'])
{
    redirect("Token mismatch");
}

if ($_POST['password'] != $_POST['confirm_password'])
{
    redirect("Passwords do not match");
}

if (strlen($_POST['password']) <= 5)
{
    redirect("Password length too short");
}

$handle = strtolower(trim($_POST['handle']));
if (!preg_match('/[A-Za-z0-9]+/', $handle))
{
    redirect("Handle must be alpha numeric.");
}

if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
{
    redirect("Bad email supplied");
}

// hash password
$salt = generate_salt();
$iterations = 100;
$hash = calculate_hash($_POST['password'], $salt, $iterations);

$stmt = $db->prepare('INSERT INTO users
                      (
                          user_handle,
                          password_hash,
                          password_salt,
                          password_iterations,
                          email,
                          balance
                      ) VALUES (:handle, :hash, :salt, :iterations, :email, 0)
                      RETURNING user_id');

$stmt->bindValue('handle', $handle, PDO::PARAM_STR);
$stmt->bindValue('hash', $hash, PDO::PARAM_LOB);
$stmt->bindValue('salt', $salt, PDO::PARAM_LOB);
$stmt->bindValue('iterations', $iterations, PDO::PARAM_INT);
$stmt->bindValue('email', $_POST['email'], PDO::PARAM_STR);

try
{
    $stmt->execute();
}
catch(PDOException $e)
{
    if($e->getCode() === '23505')
    {
        redirect("Username already registered");
    }
}

$result = $stmt->fetch(PDO::FETCH_ASSOC);

if($result === False)
{
    redirect("Failed to create user.");
}

// Everything was a success
$_SESSION['user_id'] = $result['user_id'];
redirect("Successfully registered");

